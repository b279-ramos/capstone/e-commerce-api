const auth = require("../auth.js")
const express = require("express");

const router = express.Router();
const orderController = require("../controllers/orderController.js");

router.post("/checkout", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	const data = {
		userId: userData.id,
		email: userData.email,
		reqBody: req.body
	}

    console.log(data);

	if(userData.isAdmin == false) {
		orderController.userCheckout(data).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send("You are not authorized to create orders.");
	}
});
router.get("/my-orders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	const data = {
		userId: userData.id,
		email: userData.email,
	}

	if(userData.isAdmin == false) {
		orderController.myOrders(data).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.send("Please login before doing this actions.");
	}
});

module.exports = router;