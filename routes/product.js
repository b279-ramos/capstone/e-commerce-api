const auth = require("../auth.js")
const express = require("express");

const router = express.Router();
const productController = require("../controllers/productController.js");

router.post("/create", auth.verify, (req, res) => {
    const decoded = auth.decode(req.headers.authorization);
    const data = {
        product: req.body,
        isAdmin: decoded.isAdmin,
        email: decoded.email
    };

	productController.addProduct(data).then(
        resultFromController => res.send(resultFromController)
    );
});
router.put("/update/:productId", auth.verify, (req, res) => {
	const decoded = auth.decode(req.headers.authorization);
    const data = {
        product: req.body,
        isAdmin: decoded.isAdmin,
        email: decoded.email
    };
	productController.updateProduct(req.params, data).then(
        resultFromController => res.send(resultFromController)
    );
});
router.get("/all", (req, res) => {
	productController.getAllProducts().then(
        resultFromController => res.send(resultFromController)
    );
});
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getOneProduct(req.params).then(
        resultFromController => res.send(resultFromController)
    );
});
router.put("/archive/:productId", auth.verify, (req, res) => {
	const decoded = auth.decode(req.headers.authorization);
    const data = {
        product: req.body,
        isAdmin: decoded.isAdmin,
        email: decoded.email
    };
	productController.archiveProduct(req.params, data).then(
        resultFromController => res.send(resultFromController)
    );
});
module.exports = router;