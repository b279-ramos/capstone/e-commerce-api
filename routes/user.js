const auth = require("../auth.js")
const express = require("express");

const router = express.Router();
const userController = require("../controllers/userController.js");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(
        resultFromController => res.send(resultFromController)
    );
});
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(
        resultFromController => res.send(resultFromController)
    );
});
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(
        resultFromController => res.send(resultFromController)
    );
});
router.post("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId : userData.id}).then(
        resultFromController => res.send(resultFromController)
    );
});
// router.post("/set/admin", auth.verify, (req, res) => {
// 	const userData = auth.decode(req.headers.authorization);
// 	const data = {
// 		userId: userData.id,
// 		email: userData.email,
// 	}

// 	if(userData.isAdmin == false) {
// 		userController.setAdmin(data).then(resultFromController => {
// 			res.send(resultFromController);
// 		});
// 	} else {
// 		res.send("You are already an admin role. please login.");
// 	}
// });

module.exports = router;