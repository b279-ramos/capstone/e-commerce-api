const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.userCheckout = (data) => {

    console.log("routes data " + data);
    console.log("product id " + data.reqBody.productId);

    return Product.findById(data.reqBody.productId).then(product => {
        
        console.log("products data " + product);
        console.log("product name " + product.name);
        console.log("product price " + product.price);
        console.log("product quantity " + data.reqBody.quantity);
        
        if(product == null) {

            return "error";
        
        } else {

            let newOrder = new Order({
                userId: data.userId,
                products: {
                    productId: data.reqBody.productId,
                    quantity: data.reqBody.quantity
                },
                totalAmount: product.price * data.reqBody.quantity
            });

            console.log("new order data" + newOrder);

            return newOrder.save().then((order, error) => {
                if (error) {
                    console.log("display error " + error);
                    return false;
                } else {
                    console.log(order);
                    return `${data.email} has ordered ${product.name} product successfully`;
                }
            });
        }
    });
}
module.exports.myOrders = (data) => {
    console.log(data);
    return Order.find({ userId: data.userId }).then(result => {
        return result;
    });
};