const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		name : reqBody.name,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10),
        mobileNo : reqBody.mobileNo
	})
	return newUser.save().then((user, error) => {
		if (error) {
			return "User not created please try again.";
		} else {
			return `${newUser.name} user account created successfully`;
		};
	});
};
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return { access : auth.createAccessToken(result) }
			} else {
				return false;
			};
		};
	});
};
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};
// module.exports.setAdmin = (data) => {
// 	return User.findById(data.userId).then(result => {
// 		result.password = "";
// 		return result;
// 	});
// };
