const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.addProduct = (data) => {
	console.log(data.isAdmin);
	if(data.isAdmin){
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})
		return newProduct.save().then((product, error) => {
			if(error) {
				return error;
			}
			return `${data.email} has created ${newProduct.name} successfully`;
		})
	}
	let message = Promise.resolve("You don't have the access rights to do this action.");
	return message.then((value) => {
		return value
	});
}
module.exports.updateProduct = (reqParams, data) => {
	console.log(data.isAdmin);
	if(data.isAdmin){
		let updatedProduct = {
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		}
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {
			if(error){
				return false;
			}else{
				return `${data.email} has updated ${updatedProduct.name} successfully`;
			}
		})
	}
	let message = Promise.resolve("You don't have the access rights to do this action.");
	return message.then((value) => {
		return value
	})
	
}
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
}
module.exports.getOneProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result =>{
		return result;
	})
}
module.exports.archiveProduct = (reqParams, data) => {
	console.log(data.isAdmin);
	if(data.isAdmin){
		let archivedProduct = {
			isActive: "false"
		}
		return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((course, error) => {
			if(error){
				return false;
			}else{
				return `${data.email} has deleted a product successfully`;
			}
		})
	}
	let message = Promise.resolve("You don't have the access rights to do this action.");
	return message.then((value) => {
		return value
	})
	
}